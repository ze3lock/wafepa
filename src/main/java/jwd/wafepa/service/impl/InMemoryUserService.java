package jwd.wafepa.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import jwd.wafepa.model.User;
import jwd.wafepa.service.UserService;


@Service
public class InMemoryUserService implements UserService {
	
	private Map<Long, User> users = new HashMap<>();
	private long nextId = 1L;

	@Override
	public User findOne(Long id) {
		return users.get(id);
	}

	@Override
	public List<User> findAll() {
		return new ArrayList<User>(users.values());
	}

	@Override
	public User save(User usr) {
		if(usr.getId() == null) {
			usr.setId(nextId++);
		}
		
		users.put(usr.getId(), usr);
		return usr;
	}

	@Override
	public User delete(Long id) {
		User user = users.get(id);
		if(user!=null) {
			users.remove(id);
		}
		
		return user;
	}

	@Override
	public void delete(List<Long> ids) {
		for(Long id: ids){
			delete(id);
		}
	
		
	}

	@Override
	public List<User> save(List<User> users) {

		List<User> ret = new ArrayList<>();
		
		for(User usr: users){
			User saved = save(usr);	
			if(saved!=null){
				ret.add(saved);
			}
		}
		
		return ret;
	
	}

	@Override
	public List<User> findByName(String name) {

		List<User> ret = new ArrayList<>();
		
		for(User a : users.values()){
			if(name.equals(a.getFirstName())){
				ret.add(a);
			}
		}
						
		
		return ret;
	
	}

}
