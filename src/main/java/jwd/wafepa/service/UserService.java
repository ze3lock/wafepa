package jwd.wafepa.service;

import java.util.List;

import jwd.wafepa.model.Activity;
import jwd.wafepa.model.User;



public interface UserService {
	
	User findOne(Long id);
	
	List<User> findAll();
	
	User save(User usr);
	
	User delete(Long id);
	
	void delete(List<Long> ids);
	
	List<User> save(List<User> activities);
	
	List<User> findByName(String name);

}
