package jwd.wafepa.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.wafepa.model.User;
import jwd.wafepa.service.UserService;

@RestController
@RequestMapping(value="/api/users")
public class ApiUserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<User>> getUsers(){
		List<User> users = userService.findAll();
		
		
		return new ResponseEntity<>(users, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public ResponseEntity<User> getUser (@PathVariable Long id) {
		
		User user = userService.findOne(id);
		
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(user, HttpStatus.OK);
		
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	public ResponseEntity<User> delete (@PathVariable Long id){
		
		User user = userService.delete(id);
		
		if(user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		    return new ResponseEntity<>(user, HttpStatus.OK);
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<User> add (@RequestBody User user){
		User user1 = userService.save(user);
		
		return new ResponseEntity<>(user1, HttpStatus.CREATED);
	}
	
	public ResponseEntity<User> edit (@PathVariable Long id, @RequestBody User user){
		
		User user1 = userService.save(user);
		
		return new ResponseEntity<>(user1, HttpStatus.OK);
	}
	
	public void init() {
		userService.save(new User("test@test.net", "sifra", "Sasa", "Cvijanovic"));
		userService.save(new User("test2@test.net", "sifra123", "Vladimir", "Jakovljevic"));
		
//		Long id;
//		String email;
//		String password;
//		String firstName;
//		String lastName;
	}

}
